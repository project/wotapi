<?php

namespace Drupal\wotapi\Normalizer\Value;

use Drupal\Component\Assertion\Inspector;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableDependencyTrait;
use Drupal\Core\Cache\CacheableMetadata;

use Drupal\jsonapi\Normalizer\Value\CacheableNormalization as JsonApiCacheableNormalization;

/**
 * Use to store normalized data and its cacheability.
 *
 * @internal WOT:API maintains no PHP API since its API is the HTTP API. This
 *   class may change at any time and this will break any dependencies on it.
 */
class CacheableNormalization extends JsonApiCacheableNormalization{

  use CacheableDependencyTrait;

  /**
   * A normalized value.
   *
   * @var mixed
   */
  protected $normalization;

  /**
   * CacheableNormalization constructor.
   *
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $cacheability
   *   The cacheability metadata for the normalized data.
   * @param array|string|int|float|bool|null $normalization
   *   The normalized data. This value must not contain any
   *   CacheableNormalizations.
   */
  public function __construct(CacheableDependencyInterface $cacheability, $normalization) {
    assert((is_array($normalization) && static::hasNoNestedInstances($normalization)) || is_string($normalization) || is_int($normalization) || is_float($normalization) || is_bool($normalization) || is_null($normalization));
    $this->normalization = $normalization;
    $this->setCacheability($cacheability);
  }

}
