<?php

namespace Drupal\wotapi\ResourceType;

use Drupal\jsonapi\ResourceType\ResourceType as JsonApiResourceType;
/**
 * Value object containing all metadata for a WOT:API resource type.
 *
 * Used to generate routes (collection, individual, etcetera), generate
 * relationship links, and so on.
 *
 * @internal WOT:API maintains no PHP API since its API is the HTTP API. This
 *   class may change at any time and this will break any dependencies on it.
 *
 * @see \Drupal\wotapi\ResourceType\ResourceTypeRepository
 */
class ResourceType  extends JsonApiResourceType {

  /**
   * Get the resource path.
   *
   * @return string
   *   The path to access this resource type. Default: /entity_type_id/bundle.
   *
   * @see wotapi.base_path
   */
  public function getPath() {
    $entity_type_id = $this->getEntityTypeId();
    switch ($entity_type_id) {
      case "wotapi_thing":
        return "/things";

      case "wotapi_property":
        return "/properties";

      default:
        return sprintf('/%s/%s', $entity_type_id, $this->getBundle());
    }
  }

}
